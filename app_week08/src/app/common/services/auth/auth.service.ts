import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { AppError } from '../../errors/app-error';
import { BadInput } from '../../errors/bad-input';
import { BadRequest } from '../../errors/bad-request';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from '../token/token.service';
import {
  Login,
  LoginResponse,
  ProfileResponse,
  Signup,
} from '../../interfaces/auth.interfaces';

@Injectable({
  providedIn: 'root',
})
export class AuthServiceService {
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {}

  private url = 'http://sheltered-oasis-97086.herokuapp.com/auth/';

  headers = new HttpHeaders().set('content-type', 'application/json');

  login(credentials: Login) {
    return this.http
      .post<LoginResponse>(this.url + 'login', JSON.stringify(credentials), {
        headers: this.headers.append('addToken', 'false'),
      })
      .pipe(catchError(this.handleError));
  }

  logout() {
    TokenService.deleteTokens();
  }

  signup(credentials: Signup) {
    return this.http
      .post(this.url + 'signup', JSON.stringify(credentials), {
        headers: this.headers.append('addToken', 'false'),
      })
      .pipe(catchError(this.handleError));
  }

  profile() {
    return this.http
      .get<ProfileResponse>(this.url + 'profile', {
        headers: this.headers,
      })
      .pipe(catchError(this.handleError));
  }

  refreshToken(refreshToken: string): Observable<LoginResponse> {
    const refresh = {
      refreshToken: refreshToken,
    };
    return this.http.post<LoginResponse>(this.url + 'refresh', refresh);
  }

  public isAuthenticated(): boolean {
    const refresh = TokenService.getRefreshToken();
    let token = TokenService.getToken();
    if (!token) {
      if (refresh) {
        this.refreshToken(refresh).subscribe({
          next: (res) => {
            token = res.accessToken;
            TokenService.saveToken(token);
            TokenService.saveRefreshToken(res.refreshToken);
          },
        });
      }
      return true;
    }
    return this.jwtHelper.isTokenExpired(token as string);
  }

  private handleError(error: HttpErrorResponse) {
    const message: string = `Error Status: ${error?.error.statusCode}. ${error?.message}`;
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else if (error.status === 400) {
      console.log(message);
      return throwError(() => new BadRequest(error.error));
    } else if (error.status === 401) {
      console.log(message);
      return throwError(() => new BadInput(error.error));
    } else if (error.status === 404) {
      console.error(`${error.error.message}`);
    } else {
      console.error(
        `Backend returned code ${error.status}, body was: `,
        error.error
      );
    }
    return throwError(() => new AppError(error.error));
  }
}
