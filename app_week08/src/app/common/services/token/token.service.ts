import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor() {}

  static saveToken(token: string): void {
    localStorage.setItem('userToken', token);
  }

  static getToken(): string | null {
    return localStorage.getItem('userToken');
  }

  static saveRefreshToken(refreshToken: string) {
    localStorage.setItem('refreshToken', refreshToken);
  }

  static getRefreshToken(): string | null {
    return localStorage.getItem('refreshToken');
  }

  static deleteTokens() {
    localStorage.removeItem('userToken');
    localStorage.removeItem('refreshToken');
  }
}
