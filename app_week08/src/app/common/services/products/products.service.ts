import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Products } from '../../interfaces/profile.interfaces';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private url = 'http://sheltered-oasis-97086.herokuapp.com/products';
  constructor(private http: HttpClient) {}

  private headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');

  getProducts() {
    return this.http.get<Products>(this.url, {
      headers: this.headers,
    });
  }
}
