import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthServiceService } from '../auth/auth.service';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardGuard implements CanActivate {
  constructor(public auth: AuthServiceService, public router: Router) {}
  canActivate(): boolean {
    const refreshToken = TokenService.getRefreshToken();
    if (this.auth.isAuthenticated() && !refreshToken) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
