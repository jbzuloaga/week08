import { AbstractControl, ValidationErrors } from '@angular/forms';

export class PasswordsValidators {
  static matchingPasswords(control: AbstractControl): ValidationErrors | null {
    const password = control.get('password');
    const passwordConfirmation = control.get('passwordConfirmation');
    if (password?.value !== passwordConfirmation?.value) {
      return {
        notMatchingPasswords: true,
      };
    }
    return null;
  }
}
