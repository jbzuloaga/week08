export interface Products {
  products: Product[];
  pagination: Pagination;
}

export interface Pagination {
  totalPages: number;
  itemsPerPage: number;
  totalItems: number;
  currentPage: number;
  nextPage: number | null;
  previousPage: number | null;
}

export interface Product {
  id: number;
  name: string;
  price: string;
  createdAt: Date;
}
