import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AppError } from '../common/errors/app-error';
import { BadInput } from '../common/errors/bad-input';
import { Login } from '../common/interfaces/auth.interfaces';
import { AuthServiceService } from '../common/services/auth/auth.service';
import { TokenService } from '../common/services/token/token.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent {
  form;

  constructor(
    fb: FormBuilder,
    private authService: AuthServiceService,
    private route: Router,
    private _snackBar: MatSnackBar
  ) {
    this.form = fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  getControl(control: string): FormControl {
    return this.form.get(control) as FormControl;
  }

  onSubmit() {
    if (this.form.valid) {
      this.authService.login(this.form.value as Login).subscribe({
        next: (res) => {
          TokenService.saveToken(res.accessToken);
          TokenService.saveRefreshToken(res.refreshToken);
          this.route.navigate(['home']);
        },
        error: (error: AppError) => {
          console.log('error login');
          if (error instanceof BadInput) {
            this.form.setErrors({
              statusRequest: false,
            });
            this._snackBar.open('Incorrect email or password.', 'close', {
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
        },
      });
      this.form.reset();
      Object.keys(this.form.controls).forEach((key) => {
        if (key !== 'statusRequest') {
          this.form.get(key)?.setErrors(null);
        }
      });
    }
  }
}
