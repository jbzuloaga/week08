import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { Component, Inject } from '@angular/core';

@Component({
  selector: 'app-error-signup',
  templateUrl: './error-signup.component.html',
  styleUrls: ['./error-signup.component.scss'],
})
export class ErrorSignupComponent {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: string[]) {}
}
