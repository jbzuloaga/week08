import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileResponse } from '../common/interfaces/auth.interfaces';
import { Products } from '../common/interfaces/profile.interfaces';
import { AuthServiceService } from '../common/services/auth/auth.service';
import { ProductService } from '../common/services/products/products.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  userData?: ProfileResponse;
  products?: Products;
  constructor(
    private route: Router,
    private auth: AuthServiceService,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.auth.profile().subscribe({
      next: (res) => {
        this.userData = { ...res };
      },
    });
    this.productService.getProducts().subscribe({
      next: (res) => {
        this.products = { ...res };
      },
    });
  }

  logout() {
    this.auth.logout();
    this.route.navigate(['login']);
  }
}
