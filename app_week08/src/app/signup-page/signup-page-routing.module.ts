import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginSignupGuard } from '../common/services/guards/login-signup.guard';
import { SignupPageComponent } from './signup-page.component';

const routes: Routes = [
  {
    path: '',
    component: SignupPageComponent,
    canActivate: [LoginSignupGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignupPageRoutingModule {}
