import { ErrorSignupComponent } from './../error-signup/error-signup.component';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AppError } from '../common/errors/app-error';
import { BadRequest } from '../common/errors/bad-request';
import { Signup } from '../common/interfaces/auth.interfaces';
import { AuthServiceService } from '../common/services/auth/auth.service';
import { PasswordsValidators } from '../common/validators/matching-passwords.validators';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss'],
})
export class SignupPageComponent {
  form;

  constructor(
    fb: FormBuilder,
    private router: Router,
    private authService: AuthServiceService,
    private _snackBar: MatSnackBar
  ) {
    this.form = fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordConfirmation: [
        '',
        [Validators.required, Validators.minLength(8)],
      ],
    });
    this.form.addValidators(PasswordsValidators.matchingPasswords);
  }

  getControl(control: string): FormControl {
    return this.form.get(control) as FormControl;
  }

  formatCredential(): Signup {
    return {
      name: `
        ${this.getControl('firstname').value} 
        ${this.getControl('lastname').value}
      `,
      email: this.getControl('email').value,
      password: this.getControl('password').value,
      passwordConfirmation: this.getControl('passwordConfirmation').value,
    };
  }

  onSubmit() {
    if (this.form.valid) {
      this.authService.signup(this.formatCredential()).subscribe({
        next: () => {
          this.router.navigate(['login']);
        },
        error: (error: AppError) => {
          if (error instanceof BadRequest) {
            const messages = error.error?.message;
            const errorMessages = Array.isArray(messages)
              ? messages
              : [messages];
            this._snackBar.openFromComponent(ErrorSignupComponent, {
              data: errorMessages,
              verticalPosition: 'top',
              horizontalPosition: 'center',
              duration: 2000,
            });
          }
        },
      });
      this.form.reset();
      Object.keys(this.form.controls).forEach((key) => {
        this.form.get(key)?.setErrors(null);
      });
    }
  }
}
